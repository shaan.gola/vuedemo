new Vue({
    el: '#exercise',
    data: {
        show: true,
        start: true,
        newclass: "",
        healthbarwidth: '400px',
        youprogressbarwidth: 100,
        monsterprogressbarwidth: 100,
        color: 'green',
        histories: [],
        turn: true,
        attackVelocity: 10,
        specialAttackVelocity: 20,
        whoWin: ''

    },
    methods: {
        startGame: function () {
            this.start = !this.start
            this.show = !this.show
            this.whoWin = ''
        },
        attack: function () {
            var youhit = this.getRandomno(this.attackVelocity, 1),
                monsterhit = this.getRandomno(this.attackVelocity, 1)
            this.setLog(youhit, monsterhit);
        },
        specialAttack: function () {
            var youhit = this.getRandomno(this.specialAttackVelocity, 1),
                monsterhit = this.getRandomno(this.specialAttackVelocity, 1)
            this.setLog(youhit, monsterhit);

        },
        heal: function () {
            var youhit = this.getRandomno(this.attackVelocity, 1),
                monsterhit = this.getRandomno(this.attackVelocity, 1)
            if (this.youprogressbarwidth == 100 && this.monsterprogressbarwidth == 100) {
                return false;
            }
            this.setLog(youhit, monsterhit, 'heal');
        },
        giveup: function () {
            this.start = !this.start
            this.show = !this.show
            this.whoWin = ''
            this.histories = []
            this.youprogressbarwidth = 100
            this.monsterprogressbarwidth = 100
        },
        setLog: function (youhit, monsterhit, attacktype = 'attack') {
            this.youprogressbarwidth = (attacktype == 'heal') ? this.youprogressbarwidth + youhit : this.youprogressbarwidth - youhit
            this.monsterprogressbarwidth -= monsterhit
            this.histories.push('You attack ' + youhit)
            this.histories.push('Monster attack ' + monsterhit)
            if (this.youprogressbarwidth < 1) {
                this.giveup()
                this.whoWin = 'monster'

            }
            if (this.monsterprogressbarwidth < 1) {
                this.giveup()
                this.whoWin = 'you'

            }
        },
        getRandomno: function (max, min) {
            return Math.floor(Math.random() * (max - min + 1) + min);
        }


    }
});